#ifndef LABORATORY_NUMBER_3_ROTATE_PICTURE_FILE
#define LABORATORY_NUMBER_3_ROTATE_PICTURE_FILE
#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

bool openFile(FILE** file, char const *filename, char const *open_mode);
bool closeFile(FILE* closeFile);
#endif //LABORATORY_NUMBER_3_ROTATE_PICTURE_FILE
