#include "imageIO.h"

bool readerHeader(FILE* const in, struct bmp_header *  head){
    size_t t = fread(head, sizeof(struct bmp_header), 1, in);
    return t;
}

enum read_status from_bmp(FILE* const in, struct image**  picture) {
    struct bmp_header head = {0};
    if(!readerHeader(in, &head)){
        return READ_INVALID_HEADER;
    }
    *picture = createdPicture(head.biWidth, head.biHeight);

    int64_t padding = calculetPad(head.biWidth);
    size_t count = ZERO;
    for (size_t i = 0; i < head.biHeight; ++i) {
        for (size_t j = 0; j < head.biWidth; ++j) {
            if(!fread(((*picture)->currentData + count), sizeof (struct pixel), 1, in)){
                releasePicture(*picture);
                return READ_INVALID_BITS;
            }
            count++;
        }
        if(fseek(in, padding, SEEK_CUR)){
            releasePicture(*picture);
            return READ_INVALID_SIGNATURE;
        }
    }
    return READ_OK;
}

enum write_status to_bmp(FILE* const out, struct image* picture) {
    struct bmp_header head = headerGenerate(picture);
    if(!fwrite(&head, sizeof (struct bmp_header), 1, out)){
        return WRITE_ERROR;
    }
    if(fseek(out, head.bOffBits, SEEK_SET)){
        return WRITE_ERROR;
    }

    uint8_t tmp_bytes[3] = {ZERO, ZERO, ZERO};
    size_t picturePadding = calculetPad(picture->widthPicture);
    size_t count = ZERO;
    for (size_t i = 0; i < picture->heightPicture; ++i) {
        struct pixel *to_write = ((*picture).currentData + count);
        size_t writed = fwrite(to_write, sizeof (struct pixel), picture->widthPicture, out);
        if(writed < picture->widthPicture){
            return WRITE_ERROR;
        }
        count += picture->widthPicture;
        if(picturePadding != 0){
            size_t to_padding = fwrite(&tmp_bytes, sizeof (uint8_t), picturePadding, out);
            if(to_padding < picturePadding){
                return WRITE_ERROR;
            }
        }

    }
    return WRITE_OK;
}
