#include "file.h"
#include "image.h"
#include "imageIO.h"
#include <stdio.h>

int main(int32_t argc, char** argv ) {
    FILE *file_reader = NULL;
    FILE *file_writer = NULL;
    struct image *picture = NULL;
    if(!openFile(&file_reader, argv[FILEREAD], "rb")){
        fprintf(stderr, "Не получается открыть файл: %s\n", argv[FILEREAD]);
        return EXIT;
    }

    if(!openFile(&file_writer, argv[FILEWRIRE], "wb")){
        fprintf(stderr, "Не получается открыть файл: %s for writing\n", argv[FILEWRIRE]);
        return EXIT;
    }

    if(argc != ARGCHECK){
        fprintf(stderr, "Недопустимые аргументы %s ", argv[ZERO]);
        return EXIT;
    }

    enum read_status pictureReadStatus = from_bmp(file_reader, &picture);
    if(pictureReadStatus != ZERO){
        printf("Ошибка чтения: %d", pictureReadStatus);
        return pictureReadStatus;
    }

    struct image * pictureRotated = rotatePicture(picture);
    releasePicture(picture);

    enum write_status pictureWriteStatus = to_bmp(file_writer, pictureRotated);
    if(pictureWriteStatus != WRITE_OK){
        printf("Ошибка записи: %d", pictureWriteStatus);
        releasePicture(pictureRotated);
        return pictureWriteStatus;
    }

    releasePicture(pictureRotated);

    if(!closeFile(file_reader)){
        printf("Ошибка закрытия файла");
        return -1;
    }

    return 0;
}
