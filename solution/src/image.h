#ifndef LABORATORY_NUMBER_3_ROTATE_PICTURE
#define LABORATORY_NUMBER_3_ROTATE_PICTURE
#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

struct __attribute__((packed)) pixel { uint8_t red, gren, blue; };

struct image {
    uint64_t widthPicture, heightPicture;
    struct pixel* currentData;
};
struct image* rotatePicture(struct image const *picture);

struct bmp_header headerGenerate(struct image const *picture);

struct image* createdPicture(uint64_t widthPicture, uint64_t heightPicture);

int64_t calculetPad(size_t widthPicture);

void releasePicture(struct image *picture);

enum dataNumbers  {
    ZERO = 0,
    MAGICNUMBERS = 4,
    BMPTYPE = 0x4d42,
    BMPSIZE = 40,
    BMPPLANES = 1,
    BMPBITCOUNT= 24,
    ARGCHECK= 3,
    FILEREAD = 1,
    FILEWRIRE = 2,
    EXIT = -1,
};



#endif //LABORATORY_NUMBER_3_ROTATE_PICTURE
