#include "image.h"

struct image* createdPicture(uint64_t widthPicture, uint64_t heightPicture) {
    struct pixel *pixelsInPicture = malloc(sizeof (struct pixel) * (widthPicture * heightPicture));
    struct image * currentPicture = malloc(sizeof (struct image));
    currentPicture->widthPicture = widthPicture;
    currentPicture->currentData = pixelsInPicture;
    currentPicture->heightPicture = heightPicture;
    return currentPicture;
}

void releasePicture(struct image * picture) {
    if (picture != NULL) {
        picture->widthPicture = 0;
        picture->heightPicture = 0;
        free(picture->currentData);
        free(picture);
    }
}


struct image *rotatePicture(struct image const * picture) {
    size_t count = ZERO;
    struct image* upsideDownPicture = createdPicture(picture->heightPicture, picture->widthPicture);
    for (size_t i = ZERO; i < picture->widthPicture; i++) {
        for (size_t j = ZERO; j < picture->heightPicture; j++) {
            size_t index = i + (picture->heightPicture - j - 1) * picture->widthPicture;
            struct pixel pixelInPicture = picture->currentData[index];
            upsideDownPicture->currentData[count] = pixelInPicture;
            count++;
        }
    }
    return upsideDownPicture;
}

int64_t calculetPad(size_t widthPicture){
    if(widthPicture % MAGICNUMBERS == ZERO){
        return ZERO;
    }
    int64_t a = (int64_t )((widthPicture * sizeof(struct pixel)) % MAGICNUMBERS);
    return MAGICNUMBERS - a;
}

size_t image_size(const struct image *  picture){
    size_t a = calculetPad(picture->widthPicture) * picture->heightPicture;
    return picture->widthPicture + a;
}

size_t file_size(const struct image *  picture){
    size_t a = picture->widthPicture * picture->heightPicture * sizeof(struct pixel);
    size_t b = calculetPad(picture->widthPicture) * picture->heightPicture + sizeof(struct bmp_header);
    return  a + b ;
}

struct bmp_header headerGenerate(struct image const *  picture) {
    return (struct bmp_header) {
            .bfType = BMPTYPE,
            .bfileSize = file_size(picture),
            .bfReserved = ZERO,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BMPSIZE,
            .biWidth = picture->widthPicture,
            .biHeight = picture->heightPicture,
            .biPlanes = BMPPLANES,
            .biBitCount = BMPBITCOUNT,
            .biCompression = ZERO,
            .biSizeImage = image_size(picture),
            .biXPelsPerMeter = ZERO,
            .biYPelsPerMeter = ZERO,
            .biClrUsed = ZERO,
            .biClrImportant = ZERO,
    };

}
