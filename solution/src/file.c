#include "file.h"
bool openFile(FILE **  openFile, char const *  openFileName, char const *  mode) {
    *openFile = fopen(openFileName, mode);
    return *openFile != NULL;
}


bool closeFile(FILE *  closesfile) {
    return fclose(closesfile) == 0;
}
